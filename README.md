# README #

The project is a fork of the CBC.PDESys project.

CBC.PDESys is a Python package, built on top of FEniCS, for specifying and solving large systems of nonlinear Partial Differential Equations (PDEs) with very compact, flexible and reusable code (see, e.g., http://fenicsproject.org/featured/2011/pdesys.html). The package is completely general and targets any system of PDEs. One specific feature of CBC.PDESys is that all PDESystem classes (e.g., Navier-Stokes) integrate seamlessly and can be solved together simply by adding them to a Problem class. The primary focus of CBC.PDESys today is on Computational Fluid Dynamics and turbulence modeling (RANS) through the subpackage CBC.CFD. Implemented turbulence models are currently the standard k-epsilon, Spalart Allmaras, V2F and elliptic relaxation models. CBC.CFD also comes with some highly optimized incompressible Navier-Stokes solvers. The software packages are developed at the Center for Biomedical Computing at Simula Research Laboratory in Oslo, Norway. CBC.PDESys is distributed freely in the hope that it will be useful, but without any warranty.

Link to the original project:
https://launchpad.net/cbcpdesys

### How do I get set up? ###

* Download and run.